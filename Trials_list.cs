using System.IO;
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Trials_list : MonoBehaviour
{
    static string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
    static string fullPath = Path.Combine(@folderPath, "rewards_coord_5m.txt");
    public List<float[]> float_array = new List<float[]>();
    public List<List<int>> int_array = new List<List<int>>();
    public int listIndex;
    public List<int> random_key_list = new List<int>();
    private int oldLapNumber = 0;
    private GameObject[] all_rewards;
    private float distance_path;
    private moveAvatar distance;
    void Start()
    {
        string[] lines = File.ReadAllLines(fullPath);
        foreach (string line in lines)
        {
            var trimed_line = line.Trim(new Char[] { '[', ']' });
            var parsed = Array.ConvertAll(trimed_line.Split(','), s => float.Parse(s, CultureInfo.InvariantCulture));
            float_array.Add(parsed);
        }
        foreach (var float_line in float_array)
        {
            var listlist = new List<int>();
            for (int i = 0; i < float_line.Length; i++)
            {
                listlist.Add(Mathf.RoundToInt(float_line[i]));
            }
            int_array.Add(listlist);
        }

        all_rewards = GameObject.FindGameObjectsWithTag("Reward");
        listIndex = Random.Range(0, int_array.Count - 1);
        random_key_list = int_array[listIndex];
        for (int i = 0; i < all_rewards.Length; i++)
        {
            foreach (int key in random_key_list)
            {
                if (i == key)
                {
                    all_rewards[i].GetComponent<TriggerRewards>().deliver_reward = true;
                }
            }
        }

        distance = GameObject.Find("Cube").GetComponent<moveAvatar>();
        distance_path = distance.distanceTravelled;
    }
    public void NewList()
    {
        listIndex = Random.Range(0, int_array.Count - 1);
        random_key_list = int_array[listIndex];
        for (int i = 0; i < all_rewards.Length; i++)
        {
            foreach (int key in random_key_list)
            {
                if (i == key)
                {
                    all_rewards[i].GetComponent<TriggerRewards>().deliver_reward = true;
                }
            }
        }
        oldLapNumber = distance.lapNumber;
    }
}
