using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.Linq; // for sum of array
using System.Threading; // for waiting 2s at the start

public class updatePosBelt : MonoBehaviour
{
    public static string numComPort = "7"; //5 me 6 alla Comp, 22 ordi 2P

    SerialPort stream = new SerialPort("COM" + numComPort, 57600); 
    // Movement info in X and Y
    public int[] valMovXY = new int[2];
	//public Vector3 position;


    void Start()
    {   
        try
        { 
            stream.Open();
            Debug.Log("Connected to treadmill");
            // Wait 2s for the treadmill to send intro bytes and avoid jumping
			stream.ReadTimeout = 2000;
			Thread.Sleep(2000);
			int bytes = 133;
			byte[] buffer = new byte[bytes];
			stream.Read(buffer,0,bytes);

        }
        catch
        {
            Debug.Log("Didn't connect to treadmill (Check COM port number and occupancy!)");
        }

    }

    // Invoked when a line of data is received from the serial device.
    void FixedUpdate()
    {   
    
    int bytes = stream.BytesToRead;    

     if(bytes >= 6 | bytes < 20)
     // byte < 20 to avoid big jump at the beguining not super clean 
     // but work for now
     {
		
        byte[] buffer = new byte[bytes];
        stream.Read(buffer,0,bytes);
		valMovXY = formatBuffer(buffer);
		
        //This block is to avoid crazy huge jumps
/*         Debug.Log(valMovXY[0].ToString() + "_"+ valMovXY[1].ToString());
		if (valMovXY[0] > 10000) {
        valMovXY[0] = 0;
		valMovXY[1] = 0;
		} */

		

		

     }
     
    }

   private int[] formatBuffer(byte[] buffer)
    {
        int lenBuff = buffer.Length;
        int itLoop = lenBuff/6; // read 6 bytes at the time
        int[] arrayXY = new int[2];
        int[] arrayValX = new int[itLoop];
        int[] arrayValY = new int[itLoop];
        int tmpValX;
        int tmpValY;

        for(int i = 0; i < itLoop; i++)
        {
            tmpValX = Combine(buffer[(i*6)+1],buffer[(i*6)+2]);
            tmpValY = Combine(buffer[(i*6)+4],buffer[(i*6)+5]);

            arrayValX[i] = AdjustvalPhenosys(tmpValX);
            arrayValY[i] = AdjustvalPhenosys(tmpValY);
        } 
        
        arrayXY[0] = arrayValX.Sum();
        arrayXY[1] = arrayValY.Sum();

        return arrayXY; 
    }

    private short Combine(byte b1, byte b2)
    {
    short combined =  (short)(b1 |  (b2 << 8)); 
    return combined;
    }

    private int AdjustvalPhenosys(int val)
    {
        if(val>=32767)
        {
            val = -(65536-val); 
        }
        return val;
    }
        
    void OnApplicationQuit()
    {
        stream.Close();
    }

}
