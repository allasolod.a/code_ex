#!/usr/bin/env python
"""Decoder for our own int16 transmission scheme."""

import logging

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

import OpenEphys

mpl.rcParams['font.size'] = 10
mpl.rcParams['font.sans-serif'] = 'Helvetica Neue'
mpl.rcParams['axes.spines.left'] = 'True'
mpl.rcParams['axes.spines.right'] = 'False'
mpl.rcParams['axes.spines.bottom'] = 'True'
mpl.rcParams['axes.spines.top'] = 'False'
mpl.rcParams['figure.constrained_layout.use'] = True

# parametrize the font format on the output .svg files
# 'none': The fonts need to be installed on the machine
# 'path': larger files and texts are not editable

mpl.rcParams['svg.fonttype'] = 'none'

# Using .svg figures in jupyter notebooks
# %config InlineBackend.figure_format = 'svg'

# logging configuration
logging.basicConfig(level=logging.INFO)

VOLT_MAX = 5.0 * 0x0dff / 0x0fff
DELTA_V = VOLT_MAX / 16.0


LEVELS_CTR = np.linspace(DELTA_V, VOLT_MAX, 16)


# Error detecting code
# 4-bit CRC using the polynomial x^4 + x + 1, 0x13.
# See Wikipedia https://en.wikipedia.org/wiki/Cyclic_redundancy_check for the
# choice of the polynomial

# The implementation is base of the stackoverflow
# https://stackoverflow.com/questions/15169387/definitive-crc-for-c
# This implementation is non-reflected, processing the least-significant bit of
# the input first, has an initial CRC register value of 0x00. As a result the
# CRC of an empty string, and therefore the initial CRC value, is zero.
#
# The standard description of this CRC is:
# width=4 poly=0x13 init=0x00 refin=true refout=true
# name="CRC-4/ITU"
crc4_table = np.array(
    (
        0x0, 0x3, 0x1, 0x2, 0x2, 0x1, 0x3, 0x0,
        0x3, 0x0, 0x2, 0x1, 0x1, 0x2, 0x0, 0x3,
    ),
    dtype=np.uint8,
)


# Return the CRC-4 of data (uint32) applied to the seed crc. This permits the
# calculation of a CRC a chunk at a time, using the previously returned value
# for the next seed. If data is NULL, then return the initial seed. See the
# test code for an example of the proper usage.
def crc4(crc: np.uint8, data: np.uint32):
    """Compute the CRC-4.

    Args:
        crc (np.uint8): Initial CRC if checking more than data.
        data (np.uint32): Chain to compute the CRC upon.

    Returns:
        np.uint8: the CRC.

    """
    crc &= 0xf
    for i in np.arange(4):
        masked = (data >> i * 4) & 0xf
        crc = crc4_table[crc ^ masked]
    return crc


def decoder(signal: np.ndarray):
    """Decode the output of OpenEphys.

    Args:
        signal (ndarray): the chunk of OpenEphys continuous signal.

    Returns:
        np.uint16: the decoded uint16 integer.

    """
    length = signal.shape[0] / 5

    out_lev = 0
    for i in np.arange(5):
        sta = int(i * length)
        sto = int((i + 1) * length)
        levels = signal[sta:sto]
        level = np.argmin(np.abs(LEVELS_CTR - np.median(levels)))
        out_lev += level << i * 4
    crc_read = (out_lev >> 4 * 4) & 0xff
    out_lev = (out_lev & 0xffff).astype(np.int16)
    crc_comp = crc4(0, out_lev)
    if crc_read != crc_comp:
        logging.error('Wrong CRC!')
    return out_lev


def main():
    """Launch the decoding."""
    data_behav = OpenEphys.load('testfq5000_100_ADC2.continuous')

    behav = data_behav['data']
    baseline = behav < DELTA_V / 2

    ind_baseline = np.nonzero(baseline)[0]
    behav = behav[ind_baseline[0]:ind_baseline[-1]]

    start_trans = (behav[:-1] < DELTA_V / 2) & (behav[1:] > DELTA_V / 2)
    stop_trans = (behav[:-1] > DELTA_V / 2) & (behav[1:] < DELTA_V / 2)

    start_ind = np.nonzero(start_trans)[0] + 1
    stop_ind = np.nonzero(stop_trans)[0] + 1

    trans_range = np.stack((start_ind, stop_ind), axis=-1)

    positions = np.zeros(trans_range.shape[0])

    # for i, rang in enumerate(trans_range[1678:1681]):
    for i, rang in enumerate(trans_range):
        out_lev = decoder(behav[rang[0]:rang[1]])
        positions[i] = out_lev

    fig, ax = plt.subplots()
    ax.plot(positions)
    fig.suptitle('Decoded position for each transmission')
    ax.set_xlabel('INT16 transmission index')
    ax.set_ylabel('Decoded position')
    fig.savefig('position_time.svg')

    fig, ax = plt.subplots(3, 1, figsize=(6, 4))
    ax[0].plot(behav[500000:502000])
    ax[0].set_title('Transmission extract')
    ax[0].set_xlabel('Transmission index')
    ax[0].set_ylabel('Voltage (V)')
    ax[1].hist(behav[behav < DELTA_V / 2], bins=500)
    ax[1].set_title('Baseline histogram')
    ax[1].set_xlabel('Voltage(V)')
    ax[1].set_ylabel('Counts')
    ax[2].hist(behav[behav > DELTA_V / 2], bins=500)
    ax[2].scatter(LEVELS_CTR, 25000 * np.ones(len(LEVELS_CTR)))
    ax[2].set_title('Signal histogram')
    ax[2].set_xlabel('Voltage(V)')
    ax[2].set_ylabel('Counts')
    fig.savefig('hist_voltage.svg')
    plt.show()


if __name__ == '__main__':
    # execute only if run as a script
    main()
