﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRewards : MonoBehaviour
{
    [Header("The arduinOut Object")]
    public GameObject serialOutput;

    [Header("The Avatar we have to look at")]
    public Rigidbody Avatar;

    private float distToTrig = 1;

    private GameObject[] all_rewards;
    public bool deliver_reward;
    private ArduinoSerial serialArduinout;
    private int oldLapNumber = 0;
    private string stringToSend = "2";

    void Start()
    {
        serialArduinout = serialOutput.GetComponent<ArduinoSerial>();
    }

    void FixedUpdate()
    {
        float curDist = Vector3.Distance(transform.position, Avatar.position);
        if (curDist < distToTrig & deliver_reward == true)
        {
            serialArduinout.arduinOut.WriteLine(stringToSend);
        }

        //if (curDist < distToTrig & deliver_reward == true)
        //{
        //    if (timeInZone > 5)
        //    {
        //        serialArduinout.arduinOut.WriteLine(stringToSend);
        //        timeInZone = 0;
        //    }
        //    else
        //    {
        //        timeInZone += Time.deltaTime;
        //    }
        //}

    }
}