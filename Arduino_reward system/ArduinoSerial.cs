using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System.Linq;


public class ArduinoSerial : MonoBehaviour
{
    static string defPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
    static string fullPath = Path.Combine(@defPath, "VRdata");
    static string stringDate = System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
    static string fileDir = Path.Combine(fullPath, stringDate);
    static string fileLickName = Path.Combine(fileDir, stringDate + "_lick.txt");
    // Be sure that the port number is right
    public static string numComPort = "5";
    public SerialPort arduinOut = new SerialPort("COM" + numComPort, 9600);
    private GameObject serialOutput;
    private ArduinoSerial serialArduinout;
    public int offset;
    public string readout;

    void Start()
    {
        try
        {
            arduinOut.Open();
            //Debug.Log("Connected to Arduino");
            //offset = Int32.Parse(arduinOut.ReadLine());
            arduinOut.ReadTimeout = 5;
            CreateFile();
            File.AppendAllText(fileLickName, offset + System.Environment.NewLine);
        }
        catch
        {
            //Debug.Log("Connection to Arduino Out not possible, verify COM port of it is is plugged (if you forgot to plug it take a coffee pause now)");
        }
    }

    void Update()
    {

    }

    // On application close 

    void OnApplicationQuit()
    {
        arduinOut.Close();
    }

    void CreateFile()
    {
        //check if "VRdata" directory doesn't exit
        if (!Directory.Exists(fullPath))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(fullPath);
        }
        //Create data Directory if it doesn't exist
        if (!Directory.Exists(fileDir))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(fileDir);

        }

    }


}
