﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Ports;

public class TriggerRewards : MonoBehaviour
{
    [Header("The Avatar we have to look at")]
    public Rigidbody Avatar;
    [Header("Deliver reward status")]
    public bool deliver_reward;
    private int reward = 0;
    private int reward_count;
    private float reward_pos;
    private float avatar_pos;
    static string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
    static string fullPath = Path.Combine(@folderPath, "VRdata");
    static string stringDate = System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
    static string fileDir = Path.Combine(fullPath, stringDate);
    string fileRewardName = Path.Combine(fileDir, stringDate + "_Reward.txt");
    private SerialPort port;
    private string stringToSend = "2";
    public bool reward_send;
    void Start()
    {
        CreateFile();
    }
    void Update()
    {
        avatar_pos = GameObject.Find("Cube").transform.position.x;
        reward_pos = this.transform.position.x;
        port = GameObject.Find("ArduinOut").GetComponent<ArduinoSerial>().arduinOut;
        if ((avatar_pos > reward_pos) & (deliver_reward == true))
        {
            port.WriteLine(stringToSend);
            deliver_reward = false;
            Debug.Log("Delivered");
        }
    }
    void CreateFile()
    {
        // check if "VRdata" directory doesn't exit
        if (!Directory.Exists(fullPath))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(fullPath);
        }
        //Create data Directory if it doesn't exist
        if (!Directory.Exists(fileDir))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(fileDir);

            //  Write an header txt file
            //string headerTxtfile = stringDate + System.Environment.NewLine;
            //File.WriteAllText(fileRewardName, headerTxtfile);

        }
    }
}   
