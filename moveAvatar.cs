﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class moveAvatar : MonoBehaviour
{
    public GameObject MovementProvider;
    public PathCreator pathCreator;
    public EndOfPathInstruction endOfPathInstruction;
    public float distanceTravelled;
    private updatePosBelt localUpdatePos;
    public Rigidbody target;
    public float yAngle, xAngle, zAngle = (float)180.0;
    public Camera MainCamera, RightCam, LeftCam;
    private float moveSpeed = (float)0.0464; //for 50 belt turns with unity distance 3174
    public int lapNumber = 0;
    private Trials_list list_script;
    void Start()
    {
        localUpdatePos = MovementProvider.GetComponent<updatePosBelt>();
        transform.Rotate(0.0f, 0.0f, 0.0f, Space.Self);
        list_script = GameObject.Find("Keys").GetComponent<Trials_list>();
    }
    void Update()
    {
        if (lapNumber % 2 == 0)
        {
            int[] movXY = localUpdatePos.valMovXY;
            distanceTravelled += movXY[1] * moveSpeed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);

            if (distanceTravelled >= 529.96f)
            {
                lapNumber += 1;
                MainCamera.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                RightCam.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                LeftCam.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                list_script.NewList();
            }
        }
        else
        {
            int[] movXY = localUpdatePos.valMovXY;
            distanceTravelled -= movXY[1] * moveSpeed * Time.deltaTime;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
            if (distanceTravelled <= 1f)
            {
                lapNumber += 1;
                MainCamera.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                RightCam.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                LeftCam.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
                list_script.NewList();
            }
        }
    }
}
