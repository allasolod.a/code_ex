using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System.IO.Ports;
using System.IO;
using System.Linq;

public class ArduinoOpenEphys : MonoBehaviour
{
    public static string numComPort_posout = "9";
    public SerialPort arduinOut_posout = new SerialPort("COM" + numComPort_posout, 9600);

    public GameObject ArduinoOpen;
    public moveAvatar distance;
    public float distance_arduino;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            arduinOut_posout.Open();
            //Debug.Log("Connected to ArduinoUNO");
            arduinOut_posout.ReadTimeout = 100;
        }
        catch
        {

            //Debug.Log("NOT Connected to ArduinoUNO");
        }

    }

    void Update()
    {
        ArduinoOpen = GameObject.FindWithTag("Player");
        distance = ArduinoOpen.GetComponent<moveAvatar>();
        distance_arduino = (int)distance.distanceTravelled;

        //Debug.Log("Distance to Arduino " + distance_arduino);

        arduinOut_posout.WriteLine(string.Format("%d\n", distance_arduino));

    }
    void OnApplicationQuit()
    {
        arduinOut_posout.Close();
    }
}
